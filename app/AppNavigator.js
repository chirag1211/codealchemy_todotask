/**
 * This file is for stackNavigation .
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';

import LoginLanding from './component/LoginLanding';
import Home from './component/Home';

export const APPNavigator = createStackNavigator({
    LoginLanding: { screen: LoginLanding, navigationOptions: { headerShown: false } },
}, {
    initialRouteName: 'LoginLanding'
}
);

export const HomeNavigator = createStackNavigator({
    Home: { screen: Home, navigationOptions: { headerShown: false } },
}, {
    initialRouteName: 'Home'
}
);

export const LandingAppContainer = createAppContainer(APPNavigator);
export const HomeContainer = createAppContainer(HomeNavigator);

class AppNavigator extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        if (!this.props.user.isUserLoggedIn) {
            return <LandingAppContainer />
        } else {
            return <HomeContainer />
        }
    }
}

const select = (store) => {
    return store;
}

export default connect(select)(AppNavigator);
