const initialState = {
    isUserLoggedIn: false

};

const user = (state = initialState, action) => {
    switch (action.type) {
        case 'IS_USER_LOGGED_IN':
            return { ...state, isUserLoggedIn: action.isUserLoggedIn };

        default:
            return state
    }
}

export default user;
