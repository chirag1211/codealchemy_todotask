const initialState = {
    todo: []
};

const todo = (state = initialState, action) => {
    switch (action.type) {
        case 'TODO':
            return { ...state, todo: action.data };

        case 'UPDATE_TODO':
            return {
                ...state,
                todo: action.data
            };

        default:
            return state
    }
}

export default todo;
