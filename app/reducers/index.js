import { combineReducers } from 'redux'
import user from './user';
import todo from './todo';
import { toastReducer as toast } from 'react-native-redux-toast';

const rootReducer = combineReducers({
    todo,
    user,
    toast,
});

// Exports
export default rootReducer;
