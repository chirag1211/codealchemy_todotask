import React, { Component, Fragment } from 'react';
import { SafeAreaView, StyleSheet, ScrollView, View, Text, StatusBar, Button, } from 'react-native';

import AppNavigator from './AppNavigator';

import { Provider } from 'react-redux';
import configureStore from './store/ConfigureStore';
import { store, persistor } from './store/ConfigureStore';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { Toast } from 'react-native-redux-toast';

export default class App extends Component {

  componentDidMount() {
    console.disableYellowBox = true;
  }

  render() {
    that = this;

    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Fragment>
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
              <View style={{ flex: 1, flexDirection: 'column' }}>
                <AppNavigator />
              </View >
            </SafeAreaView>
          </Fragment>
          <Toast containerStyle={{ backgroundColor: 'rgba(0,0,0,.5)' }} messageStyle={{ color: 'white' }} />
        </PersistGate>
      </Provider>
    )
  }
}