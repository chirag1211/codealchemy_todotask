import React, { Component, Fragment } from 'react';
import { SafeAreaView, StyleSheet, ScrollView, View, Text, StatusBar, Button, TouchableOpacity, Image, Alert } from 'react-native';
import { connect } from 'react-redux';
import { ToastActionsCreators } from 'react-native-redux-toast';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import { GoogleSignin, statusCodes } from 'react-native-google-signin';


class LoginLanding extends Component {

    constructor(props) {
        super(props);

        this.state = {
            userInfo: null,
        };
    }

    async componentDidMount() {
        GoogleSignin.configure({

            webClientId: '22641981015-hv3t9af62kgoa2pcqd02h9koc2aakblr.apps.googleusercontent.com',
            offlineAccess: false,
        });
        await this._getCurrentUser();
    }//end componentDidMount


    //Getting currently signned In user information.
    async _getCurrentUser() {
        try {
            const userInfo = await GoogleSignin.signInSilently();
            this.setState({
                userInfo
            });
            console.log("Now current user Login ::==> " + JSON.stringify(this.state.userInfo));

        } catch (error) {
            const errorMessage =
                error.code === statusCodes.SIGN_IN_REQUIRED ? 'Please sign in :)' : error.message;

            console.log("Error while fetching getUser:===> " + errorMessage);
        }
    }//end _getCurrentUser

    //Login method using Gmail
    async _onClickGmailLogin() {
        // this.props.onClickGmailLogin();
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            console.log("Successfully Login with :===> " + JSON.stringify(userInfo))
            this.props.dispatch({
                type: 'IS_USER_LOGGED_IN',
                isUserLoggedIn: true
            })
            this.props.dispatch(ToastActionsCreators.displayInfo('Successfully signin'))
            //Alert.alert('Successfully signin');
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // sign in was cancelled
                //Alert.alert('cancelled');
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation in progress already
                //Alert.alert('in progress');
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                Alert.alert('play services not available or outdated');
            } else {
                console.log("Something went wrong :===> " + error.toString())
                //Alert.alert('Something went wrong', error.toString());
                this.setState({
                    error,
                });
            }
        }
    }//end _onClickGmailLogin()


    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', width: widthPercentageToDP('40%'), alignSelf: 'center' }}>
                        <TouchableOpacity
                            onPress={() => this._onClickGmailLogin()}
                            style={{ height: 40, width: 40, borderRadius: 20 }}>
                            <Image
                                style={{ width: 40, height: 40 }}
                                source={require('../assets/GmailLogo.png')}
                            />
                        </TouchableOpacity>
                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }
}


const select = (store) => {
    return store;
}
export default connect(select)(LoginLanding);