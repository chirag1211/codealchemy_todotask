import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Modal, SafeAreaView, FlatList, TextInput, Keyboard, ScrollView, Platform, Image } from 'react-native';
import Colors from '../constants/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { connect } from 'react-redux';
import { ToastActionsCreators } from 'react-native-redux-toast';

class TodoModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newTodo: '',
            isShowListModal: false
        }
        this.setState({ todos: this.props.list.todos })
    }

    isTodoCompleted(index) {
        let list = this.props.list;
        list.todos[index].completed = !list.todos[index].completed
        this.props.updateList(list)
    }


    toggleListModal() {
        this.setState({
            isShowListModal: !this.state.isShowListModal
        })
    }

    addTodo() {
        let list = this.props.list;
        if (this.state.newTodo.length !== 0) {
            list.todos.push({ title: this.state.newTodo, completed: false })
            this.props.updateList(list);
            this.setState({ newTodo: '' })
        } else {
            this.props.dispatch(ToastActionsCreators.displayInfo('Please enter title'))
        }
        Keyboard.dismiss()


    }

    renderTodoLists(item, index) {
        return (
            <View style={{ paddingHorizontal: wp('3%'), flexDirection: 'row', alignItems: 'center', marginVertical: hp('2%'), }}>
                <TouchableOpacity onPress={() => this.isTodoCompleted(index)}>
                    {!item.completed ?
                        <View style={styles.checkBoxView} />
                        : <View style={styles.checkBoxView}>
                            <View style={styles.filledView} />
                        </View>
                    }
                </TouchableOpacity>

                <Text style={{ marginLeft: wp('4%'), fontWeight: '400', textDecorationLine: item.completed ? 'line-through' : 'none', color: item.completed ? 'rgba(0,0,0,0.4)' : 'black' }}>{item.title}</Text>
            </View>
        )
    }

    render() {
        const list = this.props.list
        const totalTask = list.todos.length;
        const completedTask = list.todos.filter(todo => todo.completed).length

        return (
            <SafeAreaView style={{}}>
                <ScrollView >
                    <TouchableOpacity
                        onPress={() => this.props.closeModal()}
                        style={{ height: 40, width: 40, alignSelf: 'flex-end', right: wp('3%') }}>
                        <Image
                            style={{ height: 40, width: 40, alignSelf: 'center', }}
                            source={require('../assets/close.png')}
                        />
                    </TouchableOpacity>

                    <View style={{ marginTop: 20, }}>
                        <View style={{
                            alignSelf: 'stretch', justifyContent: 'flex-end', marginLeft: 64, borderBottomWidth: 3, borderBottomColor: list.color
                        }}>
                            <Text style={{ fontSize: 22, fontWeight: '600', color: Colors.black }}>{list.name}</Text>
                            <Text style={{ marginTop: 4, marginBottom: 16, color: Colors.gray }}>{completedTask} of {totalTask} tasks</Text>
                        </View>

                        <View style={{ height: hp('65%') }}>
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                style={{ marginTop: hp('5%'), marginHorizontal: wp('6%'), }}
                                data={list.todos}
                                keyExtractor={item => item.title}
                                renderItem={({ item, index }) => this.renderTodoLists(item, index)}
                                keyboardShouldPersistTaps='always'
                            />
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                                width: wp('85%'),
                                alignSelf: 'center',
                                alignItems: 'center',
                                marginVertical: hp('3%')
                            }}>
                            <TextInput
                                onChangeText={text => this.setState({ newTodo: text })}
                                value={this.state.newTodo}
                                style={{
                                    borderColor: list.color, height: 40, width: '85%',
                                    borderWidth: StyleSheet.hairlineWidth,
                                    borderRadius: 6, marginRight: wp('4%'), paddingHorizontal: wp('3%'),
                                }}
                            />

                            <TouchableOpacity
                                onPress={() => this.addTodo()}
                                style={{
                                    backgroundColor: list.color,
                                    height: 40, width: 40,
                                    borderRadius: 6,
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                <Text style={{
                                    color: 'white',
                                    fontSize: 22,
                                    fontWeight: '600',
                                    alignSelf: 'center'
                                }}>+</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </ScrollView>
            </SafeAreaView >
        )
    }
}

const styles = StyleSheet.create({
    checkBoxView: {
        height: 18,
        width: 18,
        borderRadius: 3,
        borderColor: 'black',
        justifyContent: 'center', alignItems: 'center',
        borderWidth: 2,
    },
    filledView: {
        height: '80%',
        width: '80%',
        borderColor: 'black',
        borderWidth: 6,
        alignSelf: 'center',
    }
});

const select = (store) => {
    return store;
}
export default connect(select)(TodoModal);
