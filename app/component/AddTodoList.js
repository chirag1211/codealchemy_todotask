import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, SafeAreaView, TextInput, ScrollView, Image, Keyboard } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { ToastActionsCreators } from 'react-native-redux-toast';

export default class AddTodoList extends React.Component {
    backGroundCOlorsArray = ['gray', 'purple', 'green', 'pink', 'lightblue', 'red', 'orange']
    state = {
        name: '',
        color: this.backGroundCOlorsArray[0]
    }

    createTodo = () => {
        const { name, color } = this.state;

        const list = { name, color }

        if (name.length > 0) {
            this.props.addList(list);
            this.setState({ name: '' })
            this.props.onPress();
        } else {
            this.props.dispatch(ToastActionsCreators.displayInfo('Please enter title of Todo'))
        }
    }

    renderColor() {
        return this.backGroundCOlorsArray.map(color => {
            return (
                <TouchableOpacity key={color}
                    onPress={() => this.setState({ color: color })}
                    style={{ backgroundColor: color, height: 30, width: 30, borderRadius: 4 }} />
            )
        })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, }}>
                <ScrollView>
                    <TouchableOpacity
                        onPress={this.props.onPress}
                        style={{ height: 40, width: 40, alignSelf: 'flex-end', right: wp('3%') }}>
                        <Image
                            style={{ height: 40, width: 40, alignSelf: 'center', }}
                            source={require('../assets/close.png')}
                        />
                    </TouchableOpacity>

                    <View style={{ alignSelf: 'center', width: wp('80%'), marginTop: hp('25%') }}>
                        <Text style={{ color: 'black', fontSize: 22, fontWeight: '700', alignSelf: 'center', marginBottom: hp('3%') }}>{'Create Todo List'}</Text>
                        <TextInput
                            onChangeText={text => this.setState({ name: text })}
                            style={{
                                borderWidth: StyleSheet.hairlineWidth,
                                borderColor: this.state.color,
                                borderRadius: 6,
                                height: 50,
                                marginTop: hp('1%'),
                                paddingHorizontal: wp('3%'),
                                fontSize: 18
                            }}
                            placeholder={'List Name?'}
                        />
                        <View style={{
                            justifyContent: 'space-between',
                            flexDirection: 'row',
                            marginTop: hp('2%')
                        }}>{this.renderColor()}</View>

                        <TouchableOpacity
                            onPress={() => this.createTodo()}
                            style={{ marginTop: hp('3%'), height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 6, backgroundColor: this.state.color }}>
                            <Text style={{ fontSize: 20, color: 'white', fontWeight: '500' }}>Create</Text>
                        </TouchableOpacity>

                    </View>
                </ScrollView>
            </SafeAreaView >
        )
    }
}