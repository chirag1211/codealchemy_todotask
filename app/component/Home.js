
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar, TouchableOpacity, FlatList, Modal
} from 'react-native';
import { connect } from 'react-redux';
import Colors from '../constants/Colors';
import AddTodoList from '../component/AddTodoList';
import TodoLists from '../component/TodoLists';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import _ from 'lodash';
import { GoogleSignin } from 'react-native-google-signin';
import { ToastActionsCreators } from 'react-native-redux-toast';

class Home extends Component {
    isCalled = true;
    state = {
        modalClose: false,
        lists: _.get(this.props.todo, 'todo', []),
        loading: true
    }

    componentDidMount() {
        console.disableYellowBox = true;
        console.log("this.props ", this.props);
    }

    toggleModal() {
        this.setState({
            modalClose: !this.state.modalClose
        })
    }

    renderTodoList(item) {
        return (
            <TodoLists item={item} updateList={this.updateList} />
        )
    }

    addList = (list) => {
        this.isCalled = true;
        this.setState({
            lists: [...this.state.lists, { ...list, id: this.state.lists.length + 1, todos: [] }]
        }, () => {
            this.props.dispatch({
                type: 'TODO',
                data: this.state.lists
            })
        })

    }

    updateList = list => {
        this.setState({
            lists: this.state.lists.map(item => {
                this.isCalled = true;
                return item.id === list.id ? list : item
            })
        })
    }

    componentDidUpdate(prevState, nextState) {
        if (this.isCalled) {
            this.props.dispatch({
                type: 'TODO',
                data: this.state.lists
            })
            this.isCalled = false;
        }
    }

    //Logout method using Gmail
    _signOut = async () => {
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();

            //this.setState({ userInfo: null });
            this.props.dispatch({
                type: 'IS_USER_LOGGED_IN',
                isUserLoggedIn: false
            })
            this.props.dispatch(ToastActionsCreators.displayInfo('Successfully signout'))
            console.log("Signout Succesfull...");
            console.log("There is no any user login now. UserInfo :==> " + JSON.stringify(this.state.userInfo));


        } catch (error) {
            console.log("Error while sign out gmail :==> " + error);
        }
    };

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
                <ScrollView>
                    <Modal
                        animationType='slide' visible={this.state.modalClose}
                        onRequestClose={() => this.toggleModal()}>
                        <AddTodoList
                            addList={this.addList}
                            onPress={() => this.toggleModal()}
                        />
                    </Modal>
                    <TouchableOpacity
                        onPress={() => this._signOut()}
                        style={{ backgroundColor: 'red', width: wp('20%'), alignSelf: 'flex-end', marginTop: hp('1%'), marginRight: wp('2%') }}>
                        <Text style={{ fontSize: 20, color: 'black' }}>Signout</Text>
                    </TouchableOpacity>

                    <View style={{ flexDirection: 'row', marginTop: hp('20%') }}>
                        <View style={styles.separator} />
                        <Text style={styles.heading}>
                            Todo <Text style={{ fontWeight: '300', color: Colors.blue }}>Lists</Text>
                        </Text>
                        <View style={styles.separator} />
                    </View>


                    <View style={{ marginTop: hp('5%'), marginBottom: hp('3%'), justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity
                            onPress={() => this.toggleModal()}
                            style={styles.addList}>
                            <Text style={{ fontSize: 20, alignSelf: 'center' }}>+</Text>
                        </TouchableOpacity>

                        <Text style={styles.add}>Add List</Text>
                    </View>

                    <View style={{ padding: hp('3%'), justifyContent: 'center', alignItems: 'center' }}>
                        <FlatList
                            style={{ marginBottom: hp('5%') }}
                            data={this.state.lists}
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={item => item.name}
                            renderItem={({ item }) => this.renderTodoList(item)}
                            keyboardShouldPersistTaps='always'
                        />
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    separator: {
        backgroundColor: Colors.lightBlue, height: 1, flex: 1, alignSelf: 'center'
    },
    heading: {
        fontSize: 38,
        fontWeight: '800',
        color: Colors.black,
        paddingHorizontal: wp('10%')
    },
    addList: {
        borderWidth: 2,
        borderColor: Colors.lightBlue,
        borderRadius: 10,
        height: 60, width: 60,
        alignItems: 'center', justifyContent: 'center'
    },
    add: {
        color: Colors.blue,
        fontWeight: '600',
        fontSize: 14,
        marginVertical: hp('1%')
    },
});

const select = (store) => {
    return store;
}
export default connect(select)(Home);

