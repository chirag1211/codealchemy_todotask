import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Modal, SafeAreaView, ScrollView } from 'react-native';
import Colors from '../constants/Colors';
import TodoModal from './TodoModal';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default class TodoList extends Component {
    state = {
        isShowListModal: false
    }

    toggleListModal() {
        this.setState({
            isShowListModal: !this.state.isShowListModal
        })
    }

    render() {
        const item = this.props.item

        const Completed = item.todos.filter(todo => todo.completed).length;
        const remaining = item.todos.length - Completed

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
                <ScrollView>
                    <Modal
                        visible={this.state.isShowListModal}
                        onRequestClose={() => this.toggleListModal()}
                        animationType={'slide'}
                    >
                        <TodoModal
                            list={item}
                            closeModal={() => this.toggleListModal()}
                            updateList={this.props.updateList}
                        />
                    </Modal>

                    <TouchableOpacity
                        onPress={() => this.toggleListModal()}
                        style={[styles.listContainer, { backgroundColor: item.color }]}>
                        <Text style={styles.listTitle}>{item.name}</Text>
                        <View>
                            <Text style={styles.count}>{remaining}</Text>
                            <Text style={styles.countTitle}>{'Remaining'}</Text>
                        </View>
                        <View style={{ marginVertical: hp('2%') }}>
                            <Text style={styles.count}>{Completed}</Text>
                            <Text style={styles.countTitle}>{'Completed'}</Text>
                        </View>
                    </TouchableOpacity>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    listContainer: {
        paddingVertical: hp('3%'),
        paddingHorizontal: wp('5%'),
        borderRadius: 6,
        marginHorizontal: wp('5%'),
        alignItems: 'center',
        width: wp('45%')
    },
    listTitle: {
        fontSize: 24,
        color: Colors.white,
        fontWeight: '700',
        marginBottom: hp('2%'),

    },
    count: {
        fontSize: 34,
        color: Colors.white,
        fontWeight: '300',
        alignItems: 'center',
        alignSelf: 'center',
        textAlign: 'center'
    },
    countTitle: {
        fontSize: 15,
        color: Colors.white
    }


});